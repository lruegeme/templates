---
title: paper title
subtitle: paper subtitle
author:
    - Author One
    - Author Two
toc: True
toc-depth: 2
header-includes:
    - \usepackage{fullpage}
    - \usepackage{setspace}
    - \doublespacing
tags: [nothing, nothingness]
abstract: |
  This is the abstract.

  It consists of two paragraphs.
---
foo
===

sub
---

* blub reference [@test]


sub2
---

* meh


bar
===

* duh

hello
===

![test image][i1]

END
===

[i1]: images/test.png "sample image" { width=100% }
